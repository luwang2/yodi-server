const express = require('express');
const router = express.Router();

const ticketController = require('../controllers/ticketController');

router.post('/tickets', ticketController.ticketCreate);
router.get('/tickets', ticketController.ticketGet);
router.delete('/tickets/:id', ticketController.ticketDelete); // Ensure the delete route has :id

module.exports = router;