const express = require('express');
const router = express.Router();

const transactionController = require('../controller/transaction');


router.post('/transaction', transactionController.createTransaction);
router.get('/transaction', transactionController.getAllTransactions);
router.put('/transaction/:id/confirm', transactionController.confirmTransaction);


module.exports = router;