const express = require('express');
const userController = require('./../controllers/userController');
// const authMiddleware = require('../middleware/authMiddleware');
const { sendMail } = require("../controllers/sendMail")
const router = express.Router()


router.get('/verify-token', userController.verifyToken);
// router.use(authMiddleware.protect);

router.post('/register', userController.signup)
router.post('/signin', userController.login)
router.get('/logout', userController.logout)
router.post('/verify-otp', userController.verifyOTP);
router.post('/forgotPassword', userController.forgotPassword);
router.post('/resetPassword', userController.resetPassword);

router.patch("/changePassword", userController.changePassword);


router
    .route('/')
    .get(userController.getAllUsers)

router
    .route('/:id')
    .get(userController.getUser)
    .patch(userController.updateUser)
    .delete(userController.deleteUser)


router.post("/sendmail", sendMail)

module.exports = router