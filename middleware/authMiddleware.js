const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const { promisify } = require('util');

exports.protect = async (req, res, next) => {
    try {
        let token;
        // Check if the token is provided in the Authorization header or cookies
        if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
            // Extract token from Authorization header
            token = req.headers.authorization.split(' ')[1];
        } else if (req.cookies.jwt) {
            // Extract token from cookies
            token = req.cookies.jwt;
        }

        // If token doesn't exist or is empty, return an error
        if (!token || token === '') {
            return res.status(401).json({ error: 'You are not logged in. Please log in to access this resource.' });
        }

        // Verify the token
        // const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
        // Check if the user still exists
        const user = await User.findById(decoded.id);
        if (!user) {
            console.log(user)
            return res.status(401).json({ status: 'fail', message: 'User no longer exists' });
        }

        // Populate req.user with the authenticated user's information
        req.user = user;
        // Call next middleware
        next();

    } catch (error) {
        console.log(error)
        return res.status(401).json({ error: 'Invalid token. Please log in again.' });
    }
};
