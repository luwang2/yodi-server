const mongoose = require('mongoose');

const ticketSchema = new mongoose.Schema({
    selectedDate: {
        type: Date,
        required: [true, 'Please select the date']
    },
    numTickets_1: {
        type: Number,
        default: 0
    },
    numTickets_2: {
        type: Number,
        default: 0
    },
    totalAmount: {
        type: Number,
        default: 0
    },
    name: {
        type: String,
        required: [true, 'Please enter your name']
    },
    cid: {
        type: Number,
        required: [true, 'Please enter your valid cid']
    },
    phoneNumber: {
        type: Number,
        required: [true, 'Please enter your phone number']
    },
    email: {
        type: String,
        required: [true, 'Please your email']
    },
    journalNumber: {
        type: Number,
        required: true
    },
    confirmed: {
        type: Boolean,
        default: false // Add this field if it doesn't exist
    }
});

// Create a model using the schema
const Ticket = mongoose.model('Ticket', ticketSchema);

module.exports = Ticket;
