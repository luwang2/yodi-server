const nodemailer = require('nodemailer');

// Configure the email transporter using your email service (e.g., Gmail, SMTP)
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'yodipark@gmail.com',
        pass: 'lhcl byfm oakg lavb'
    }
});

const sendTicketConfirmationEmail = (userEmail, ticketDetails) => {
    const mailOptions = {
        from: 'YODI PARK <yodipark@gmail.com>',
        to: userEmail,
        subject: 'Ticket Confirmation',
        html: `
            <hp>Dear ${ticketDetails.name}</hp>
            <p>Thank you for choosing YODI WATER PARK for your ticket booking! We are delighted to confirm your reservation for your adventure water park. We appreciate your trust in us and are excited to welcome you</p>
            <p>Here are your booking details:</p>
            <p>Name: ${ticketDetails.name}</p>
            <p>Yodi Surge Pass: ${ticketDetails.numTickets_1}</p>
            <p>Yodi Splash Pass: ${ticketDetails.numTickets_2}</p>
            <p>Total Amount: ${ticketDetails.totalAmount}</p>

        
            <p>Entry Time: 8 AM to 5 PM</p>

            <p>To make your experience seamless, please keep this confirmation email handy, either printed or accessible on your mobile device, as you may need to show it at the  entry.</p>

            <p>We look forward to providing you with a memorable experience. If you have any questions or need further assistance, please don’t hesitate to contact our support team at luwang314@gmail.com or 17592225.</p>

            <p>Thank you once again for your booking. 
            We can't wait to welcome you to YODI WATER PARK!
            .</p>
            <p>Best regards,<br>Yodi Water Park</p>

            <p>S-squad park</p>
            <p>YODI'owner</p>
            <p>+97517592225</p>
            <p>+97577444890</p>
            <p>Website</p>

            <p style="text-align: center;">
                <img src="https://res.cloudinary.com/dp4f2a1xv/image/upload/v1717099755/yodi_logo_blue_au1oz8.png" alt="Yodi Water Park Logo" style="max-width: 200px; display: block; margin: 0 auto;">
            </p>
        `
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log('Error sending email:', error);
        } else {
            console.log('Email sent:', info.response);
        }
    });
};

module.exports = { sendTicketConfirmationEmail };

