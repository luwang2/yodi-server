const cloudinary = require('cloudinary').v2;
const fs = require('fs');

cloudinary.config({
    cloud_name: 'dp4f2a1xv',
    api_key: '299876355889721',
    api_secret: 'n5e3cCcNxmCsKw8TgolRATxSHZY',
});

const uploadImagesConcurrently = async (filePaths) => {
    const uploadPromises = filePaths.map(async (filePath) => {
        try {
            const result = await cloudinary.uploader.upload(filePath, {
                folder: 'images'
            });
            fs.unlinkSync(filePath);
            return result.secure_url;
        } catch (error) {
            fs.unlinkSync(filePath);
            throw error;
        }
    });

    return Promise.all(uploadPromises);
};

const deleteImages = async (publicIds) => {
    const deletePromises = publicIds.map(publicId => cloudinary.uploader.destroy(publicId));
    return Promise.all(deletePromises);
};

module.exports = { uploadImagesConcurrently, deleteImages };