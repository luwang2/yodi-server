const transactions = require('../models/admintransactionModel');

exports.createTransaction = (req, res) => {
    const transaction = req.body;
    transaction.id = Date.now().toString();
    transaction.confirmed = false;
    transactions.push(transaction);
    res.status(201).json({ message: 'Transaction created successfully' });
};

exports.getAllTransactions = (req, res) => {
    res.status(200).json(transactions);
};

exports.confirmTransaction = (req, res) => {
    const { id } = req.params;
    const index = transactions.findIndex(transaction => transaction.id === id);
    if (index !== -1) {
        transactions[index].confirmed = true;
        res.status(200).json({ message: 'Transaction confirmed successfully' });
    } else {
        res.status(404).json({ message: 'Transaction not found' });
    }
};
