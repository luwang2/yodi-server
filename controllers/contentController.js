const Content = require('../models/contentModel');
const { uploadImagesConcurrently, deleteImages } = require('../utils/cloudinary');

const createContent = async (req, res) => {
  try {
    let imageUrls = [];
    if (req.files && req.files.length > 0) {
      const filePaths = req.files.map(file => file.path);
      imageUrls = await uploadImagesConcurrently(filePaths);
    }

    // Validate that category is present
    if (!req.body.category) {
      return res.status(400).json({ error: "Category is required" });
    }

    const newContent = new Content({
      text: req.body.text || "",
      images: imageUrls,
      category: req.body.category // Ensure category is handled
    });

    await newContent.save();
    res.status(201).json(newContent);
  } catch (error) {
    console.error(error); // Log the error for debugging
    res.status(500).json({ error: error.message });
  }
};


const updateContent = async (req, res) => {
  try {
    let imageUrls = [];
    if (req.files && req.files.length > 0) {
      const filePaths = req.files.map(file => file.path);
      imageUrls = await uploadImagesConcurrently(filePaths);
    }

    const updateData = {
      text: req.body.text || "",
      category: req.body.category // Update the category field
    };

    // Update images if new images are uploaded
    if (imageUrls.length > 0) {
      updateData.images = imageUrls;
    }

    // Find and update the content
    const updatedContent = await Content.findByIdAndUpdate(req.params.id, updateData, { new: true });

    res.status(200).json(updatedContent);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};


const getContent = async (req, res) => {
  try {
    const category = req.query.category;
    const query = category ? { category } : {};
    const contents = await Content.find(query);
    res.status(200).json(contents);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const deleteContent = async (req, res) => {
  try {
    // Log the incoming request
    console.log(`Attempting to delete content with ID: ${req.params.id}`);

    // Attempt to find the content by ID
    const content = await Content.findById(req.params.id);
    if (!content) {
      console.error(`Content not found for ID: ${req.params.id}`);
      return res.status(404).json({ error: "Content not found" });
    }

    // Log the content details before deletion
    console.log(`Content found: ${JSON.stringify(content)}`);

    // Delete images from Cloudinary if they exist
    if (content.images && content.images.length > 0) {
      try {
        await deleteImages(content.images);
        console.log(`Images deleted from Cloudinary: ${content.images}`);
      } catch (imageDeleteError) {
        console.error("Error deleting images from Cloudinary:", imageDeleteError);
        return res.status(500).json({ error: "Failed to delete images from Cloudinary" });
      }
    }

    // Attempt to remove the content document from the database
    try {
      await Content.findByIdAndDelete(req.params.id);
      console.log(`Content with ID: ${req.params.id} deleted successfully`);
      res.status(200).json({ message: "Content deleted successfully" });
    } catch (dbDeleteError) {
      console.error("Error deleting content from database:", dbDeleteError);
      res.status(500).json({ error: "Failed to delete content from database" });
    }
  } catch (error) {
    console.error("Unexpected error occurred while deleting content:", error);
    res.status(500).json({ error: "An unexpected error occurred while deleting the content" });
  }
};

module.exports = { createContent, updateContent, getContent, deleteContent };