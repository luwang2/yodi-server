const { CLIENT_RENEG_LIMIT } = require('tls');
const User = require('./../models/userModel')
const jwt = require('jsonwebtoken')
const promisify = require('util').promisify;
const sendEmail = require('./../utils/email'); // Adjust the path as necessary
const bcrypt = require('bcryptjs');

// Generate OTP
const generateOTP = () => {
    return Math.floor(100000 + Math.random() * 900000).toString();
};


exports.getAllUsers = async (req, res, next) => {
    try {
        const user = await User.find()
        res.status(200).json({ data: user, status: 'success' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.getUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        res.json({ data: user, status: 'success' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}



exports.updateUser = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true });
        if (!user) {
            return res.status(404).json({ status: 'fail', message: 'User not found' });
        }
        res.json({ data: user, status: 'success' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.deleteUser = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        res.json({ data: user, status: 'success' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

const signToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN,
    })
}

const createSendToken = (user, statusCode, res) => {
    const token = signToken(user._id)
    const cookieOptions = {
        expires: new Date(
            Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000,
        ),
        httpOnly: true,
    }
    res.cookie('jwt', token, cookieOptions)

    res.status(statusCode).json({
        status: "success",
        token,
        data: {
            user
        }
    })
}


exports.signup = async (req, res) => {
    try {
        const { email, firstName, lastName, password } = req.body;
        console.log('Request body:', req.body); // Add this line for debugging
        // Check if the email already exists
        let user = await User.findOne({ email });
        console.log('User found by email:', user); // Add this line for debugging
        if (user) {
            if (user.otp && user.otpExpires > Date.now()) {
                return res.status(400).json({
                    status: 'failed',
                    message: 'Email already exists and OTP is still valid'
                });

            } else {
                console.log('Regenerating OTP for existing user...');
                // Regenerate OTP for the existing user
                const otp = generateOTP();
                console.log('Generated OTP:', otp); // Add this line for debugging
                user.firstName = firstName;
                user.lastName = lastName;
                user.password = password;
                user.otp = otp;
                user.otpExpires = Date.now() + 10 * 60 * 1000; // OTP expires in 10 minutes
                await user.save();

                const message = `Your OTP for email verification is ${otp}. It is valid for 10 minutes.`;
                console.log('Sending email with OTP:', message); // Add this line for debugging
                await sendEmail({
                    email: user.email,
                    subject: 'Email Verification',
                    message
                });

                return res.status(200).json({
                    status: 'success',
                    message: 'OTP resent to email!'
                });

            }
        };
        console.log('Creating new user...');

        const otp = generateOTP();
        const otpExpires = Date.now() + 10 * 60 * 1000; // OTP expires in 10 minutes

        user = await User.create({
            firstName,
            lastName,
            email,
            password,
            otp,
            otpExpires
        });
        console.log('New user created:', user); // Add this line for debugging

        const message = `Your OTP for email verification is ${otp}. It is valid for 10 minutes.`;
        console.log('Sending email with OTP:', message); // Add this line for debugging
        await sendEmail({
            email: req.body.email,
            subject: 'Email Verification',
            message
        });

        // Remove sensitive data from the user object
        const userWithoutPassword = user.toObject();
        delete userWithoutPassword.password;

        // Send the token and user data in the response
        console.log('Sending response...');
        res.status(201).json({
            status: 'success',
            message: 'OTP sent to email!',
            data: userWithoutPassword
        });

    } catch (err) {
        res.status(500).json({ error: err.message });
        console.log(err)
    }
};


// Lungten
exports.verifyToken = async (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(401).json({ status: 'fail', message: 'No token provided' });
        }

        const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
        const user = await User.findById(decoded.id);

        if (!user) {
            return res.status(401).json({ status: 'fail', message: 'User not found' });
        }

        res.status(200).json({
            status: 'success',
            data: { user }
        });
    } catch (err) {
        res.status(401).json({ status: 'fail', message: 'Invalid token' });
    }
};



exports.verifyOTP = async (req, res) => {
    try {
        const { email, otp } = req.body;

        const user = await User.findOne({
            email,
            otp,
            otpExpires: { $gt: Date.now() } // Ensure OTP is not expired
        });

        if (!user) {
            return res.status(400).json({
                status: 'fail',
                message: 'OTP is invalid or has expired'
            });
        }

        user.otp = undefined;
        user.otpExpires = undefined;
        await user.save();

        createSendToken(user, 200, res);

    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.resendOTP = async (req, res) => {
    try {
        const { email } = req.body;

        // Check if the user exists and OTP has expired
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({
                status: 'fail',
                message: 'User does not exist'
            });
        }

        if (!user.otp || user.otpExpires < Date.now()) {
            // Generate new OTP and update user
            const newOtp = generateOTP();
            user.otp = newOtp;
            user.otpExpires = Date.now() + 10 * 60 * 1000; // OTP expires in 10 minutes
            await user.save();

            // Send the new OTP to the user's email
            const message = `Your new OTP for email verification is ${newOtp}. It is valid for 10 minutes.`;
            await sendEmail({
                email: user.email,
                subject: 'Email Verification - New OTP',
                message
            });

            return res.status(200).json({
                status: 'success',
                message: 'New OTP sent to email!'
            });
        } else {
            return res.status(400).json({
                status: 'fail',
                message: 'Current OTP is still valid'
            });
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.login = async (req, res) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            return res.status(401).send('Please provide an email and password!');
        }

        const user = await User.findOne({ email }).select('+password');
        if (!user) {
            console.log('User not found with email:', email);
            return res.status(401).send("Invalid credentials!");
        }

        const isCorrectPassword = await user.correctPassword(password, user.password);
        if (!isCorrectPassword) {
            console.log(`Invalid password: ${password} for user ${email}`);
            return res.status(401).send("Invalid credentials!");
        }
        //lungten
        const token = signToken(user._id);

        res.status(200).json({
            status: 'success',
            token,
            data: {
                user
            }
        })


    } catch (err) {
        console.error('Error during login:', err);
        // res.status(500).json({ error: err.message });
        res.status(500).json({
            status: 'fail',
            message: err.message
        });
    }
};


exports.logout = (req, res) => {
    res.clearCookie('jwt');
    res.status(200).json({ status: 'success' });
};



exports.forgotPassword = async (req, res) => {
    try {
        const { email } = req.body;
        const user = await User.findOne({ email });

        if (!user) {
            return res.status(404).json({ status: 'fail', message: 'User not found' });
        }

        // Generate OTP
        const otp = generateOTP();
        user.otp = otp;
        user.otpExpires = Date.now() + 10 * 60 * 1000; // OTP expires in 10 minutes
        await user.save();

        // Send OTP via email
        const message = `Your OTP for password reset is ${otp}. It is valid for 10 minutes.`;
        await sendEmail({
            email: user.email,
            subject: 'Password Reset OTP',
            message
        });

        res.status(200).json({ status: 'success', message: 'OTP sent to email' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.resetPassword = async (req, res) => {
    const { token, newPassword, confirmPassword } = req.body;

    if (!token) {
        return res.status(400).json({ messages: "Token must be provided" });
    }

    if (newPassword !== confirmPassword) {
        return res.status(400).json({ messages: "Password didn't match" });
    }

    try {
        console.log("Verifying token...");
        const decoded = jwt.verify(token, process.env.JWT_SECRET);

        console.log("Decoded token:", decoded);

        const user = await User.findById(decoded.id);

        if (!user) {
            console.log("User not found with ID:", decoded.id);
            return res.status(404).json({ message: "User not found" });
        }

        console.log("Updating user password...", newPassword);

        user.password = await bcrypt.hash(newPassword, 12); // Hash the new password
        console.log("Hashed password:", user.password);
        await user.save();


        res.status(200).json({ message: "Password reset successful" });
    } catch (error) {
        console.error("Server error:", error);
        res.status(500).json({ messages: "Server error", error: error.message });
    }
};

exports.changePassword = async (req, res, next) => {
    try {
        const { currentPassword, password, passwordConfirm } = req.body;

        // Validate request body
        if (!currentPassword || !password || !passwordConfirm) {
            return res.status(400).json({
                status: "Failed",
                message:
                    "Please provide current password, new password, and confirm password",
            });
        }

        // Check if new password and confirmation match
        if (password !== passwordConfirm) {
            return res.status(400).json({
                status: "Failed",
                message: "Passwords do not match",
            });
        }

        // Validate new password length
        if (password.length < 8) {
            return res.status(400).json({
                status: "Failed",
                message: "New password must be at least 8 characters long",
            });
        }

        // Ensure the user is authenticated and loaded by the middleware
        const user = await User.findById(req.user._id).select("+password");

        if (!user) {
            return res.status(404).json({
                status: "Failed",
                message: "User not found",
            });
        }

        // Verify the current password
        const verified = await bcrypt.compare(currentPassword, user.password);
        if (!verified) {
            return res.status(400).json({
                status: "Failed",
                message: "Invalid current password",
            });
        }

        // Update the password
        user.password = password;
        user.passwordConfirm = passwordConfirm;

        await user.save();

        // Generate a new JWT token
        // const jwtToken = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
        //     expiresIn: "30d",
        // });

        // Respond with success
        res.status(200).json({
            status: "Success",
            // results: { jwtToken },
        });
    } catch (error) {
        console.error("Error:", error);
        res.status(500).json({
            status: "Failed",
            message: "An error occurred while changing password",
            error: error.message,
        });
    }
};







